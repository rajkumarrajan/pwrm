package com.pwrmassociation.sribarati.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pwrmassociation.sribarati.API.Api;
import com.pwrmassociation.sribarati.Pojo.SignInDataPojo;
import com.pwrmassociation.sribarati.Pojo.SignInPojo;
import com.pwrmassociation.sribarati.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.pwrmassociation.sribarati.Service.Service.createService;

public class SignInActivity extends AppCompatActivity {

    Button signup,signin;

    EditText UserID, Password;

    private ProgressDialog progressBar;
    private int progressBarStatus = 0;

    List<SignInDataPojo> signInDataPojos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        UserID = (EditText) findViewById(R.id.userID);
        Password = (EditText) findViewById(R.id.Password);

        signup = (Button)findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(i);
            }
        });

        signin = (Button) findViewById(R.id.login_in);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!CheckEmpty(UserID) && !CheckEmpty(Password)) {

                    progressBar = new ProgressDialog(SignInActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Loading ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    progressBarStatus = 0;

                    //    opt=auth&Id=EmailId&Password=Password

                    Api apiService = createService(Api.class);

                    Call<SignInPojo> call = apiService.SIGN_IN_POJO_CALL("auth",
                            UserID.getText().toString(), Password.getText().toString());

                    call.enqueue(new retrofit2.Callback<SignInPojo>() {
                        @Override
                        public void onResponse(Call<SignInPojo> call, retrofit2.Response<SignInPojo> response) {
                            int response_code = response.code();
                            if (response_code == 200) {
                                String status = response.body().getSuccess();
                                if (status.equals("true")) {

                                    signInDataPojos = new ArrayList<>();
                                    SignInDataPojo[] item = response.body().getData();

                                    for (SignInDataPojo anItem : item) {
                                        String UserName = anItem.getUserName();
                                        String Password = anItem.getPassword();
                                        String MobileNo = anItem.getMobileNo();
                                        String EmailID = anItem.getEmailId();
                                    }
                                    finish();

                                    Intent i = new Intent(SignInActivity.this, MenuActivity.class);
                                    startActivity(i);
                                    progressBar.dismiss();
                                } else {

                                    Toast.makeText(SignInActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    progressBar.dismiss();
                                }

                            } else {
                                Toast.makeText(SignInActivity.this, "Unable to connect to server", Toast.LENGTH_SHORT).show();
                                progressBar.dismiss();
                            }
                            progressBar.dismiss();
                        }

                        @Override
                        public void onFailure(Call<SignInPojo> call, Throwable t) {
                            try {
                                Toast.makeText(SignInActivity.this, "Server Problem, Please try again later", Toast.LENGTH_SHORT).show();

                            } catch (NullPointerException e) {
                            }
                            progressBar.dismiss();
                        }
                    });


                }
                else {
                    Toast.makeText(SignInActivity.this, "Fill all the fields", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public boolean CheckEmpty(EditText editText){
        return editText.getText().toString().isEmpty();
    }

}
