package com.pwrmassociation.sribarati.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pwrmassociation.sribarati.API.Api;
import com.pwrmassociation.sribarati.Fragment.Ac_SerFragment;
import com.pwrmassociation.sribarati.Pojo.SignUpPojo;
import com.pwrmassociation.sribarati.R;

import retrofit2.Call;

import static com.pwrmassociation.sribarati.Service.Service.createService;

public class SignUpActivity extends AppCompatActivity {

    Button signUP;

    EditText Name, MobileNumber, EMailID, Password, RePassword;

    private ProgressDialog progressBar;
    private int progressBarStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Sign Up");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Name = (EditText) findViewById(R.id.userID);
        MobileNumber = (EditText) findViewById(R.id.mobileNumber);
        EMailID = (EditText) findViewById(R.id.mailID);
        Password = (EditText) findViewById(R.id.password);
        RePassword = (EditText) findViewById(R.id.repassword);

        signUP = (Button) findViewById(R.id.sign_up);
        signUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressBar = new ProgressDialog(SignUpActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Loading ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                progressBarStatus = 0;

                if (!CheckEmpty(Name) && !CheckEmpty(MobileNumber) && !CheckEmpty(EMailID)
                        && !CheckEmpty(Password) && !CheckEmpty(RePassword)) {

                    if (Password.getText().toString().equals(RePassword.getText().toString()) ) {
                        if(isValidEmail(EMailID.getText().toString())) {


//                opt=signup&UserName=UserName&Password=Password&MobileNo=9566019013&EmailId=rajesh@gmail.com

                            Api apiService = createService(Api.class);

                            Call<SignUpPojo> call = apiService.SIGN_UP_POJO_CALL("signup",
                                    Name.getText().toString(), Password.getText().toString(), MobileNumber.getText().toString(),
                                    EMailID.getText().toString());

                            call.enqueue(new retrofit2.Callback<SignUpPojo>() {
                                @Override
                                public void onResponse(Call<SignUpPojo> call, retrofit2.Response<SignUpPojo> response) {
                                    int response_code = response.code();
                                    if (response_code == 200) {
                                        String status = response.body().getSuccess();
                                        if (status.equals("true")) {
                                            finish();
                                            Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                            Intent i = new Intent(SignUpActivity.this, SignInActivity.class);
                                            startActivity(i);
                                            progressBar.dismiss();
                                        } else {

                                            Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                            progressBar.dismiss();
                                        }

                                    } else {
                                        Toast.makeText(SignUpActivity.this, "Unable to connect to server", Toast.LENGTH_SHORT).show();
                                        progressBar.dismiss();
                                    }
                                    progressBar.dismiss();
                                }

                                @Override
                                public void onFailure(Call<SignUpPojo> call, Throwable t) {
                                    try {
                                        Toast.makeText(SignUpActivity.this, "Server Problem, Please try again later", Toast.LENGTH_SHORT).show();

                                    } catch (NullPointerException e) {
                                    }
                                    progressBar.dismiss();
                                }
                            });
                        }else {
                            Toast.makeText(SignUpActivity.this, "Enter valid E-Mail ID", Toast.LENGTH_SHORT).show();
                            progressBar.dismiss();
                        }
                    }
                    else {
                        Toast.makeText(SignUpActivity.this, "Both password are not matching", Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();
                    }
                }else {
                    Toast.makeText(SignUpActivity.this, "Enter Value for all the fields", Toast.LENGTH_SHORT).show();
                    progressBar.dismiss();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public boolean CheckEmpty(EditText editText){
        return editText.getText().toString().isEmpty();
    }

    public final static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
