package com.pwrmassociation.sribarati.Pojo;

/**
 * Created by Rajkumar Rajan on 12-01-2018.
 */

public class SignInPojo {
    private SignInDataPojo[] data;

    private String success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public SignInDataPojo[] getData ()
    {
        return data;
    }

    public void setData (SignInDataPojo[] data)
    {
        this.data = data;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", success = "+success+"]";
    }
}
