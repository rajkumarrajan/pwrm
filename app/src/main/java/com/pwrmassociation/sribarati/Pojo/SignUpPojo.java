package com.pwrmassociation.sribarati.Pojo;

/**
 * Created by Rajkumar Rajan on 12-01-2018.
 */

public class SignUpPojo
{
    private String Message;

    private String success;

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Message = "+Message+", success = "+success+"]";
    }
}