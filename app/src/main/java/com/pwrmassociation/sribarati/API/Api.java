package com.pwrmassociation.sribarati.API;

import com.pwrmassociation.sribarati.Pojo.SignInPojo;
import com.pwrmassociation.sribarati.Pojo.SignUpPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Rajkumar on 09-01-2017.
 */

public interface Api {

    //    opt=signup&UserName=UserName&Password=Password&MobileNo=9566019013&EmailId=rajesh@gmail.com
    @GET("service.php")
    Call<SignUpPojo> SIGN_UP_POJO_CALL(@Query("opt") String auth, @Query("UserName") String UserName,
                                     @Query("Password") String Password,
                                       @Query("MobileNo") String MobileNo,
                                       @Query("EmailId") String EmailId);

//    opt=auth&Id=EmailId&Password=Password
    @GET("service.php")
    Call<SignInPojo> SIGN_IN_POJO_CALL(@Query("opt") String auth, @Query("Id") String EmailId,
                                       @Query("Password") String Password);

}
