package com.pwrmassociation.sribarati.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pwrmassociation.sribarati.Activity.SignUpActivity;
import com.pwrmassociation.sribarati.Fragment.Ac_SerFragment;
import com.pwrmassociation.sribarati.Fragment.BankFragment;
import com.pwrmassociation.sribarati.Fragment.SignUpFragment;
import com.pwrmassociation.sribarati.R;

/**
 * Created by Rajkumar Rajan on 21-12-2017.
 */

public class ServiceCostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private String[] activity;
    private String[] cost;
    private String[] des;
    private Context context;

    public ServiceCostAdapter(Context context, String[] activity, String[] cost, String[] des) {
        this.context = context;
        this.activity = activity;
        this.cost = cost;
        this.des = des;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView act,co,de;
        Button button;
        private ViewHolder(View itemView) {
            super(itemView);
            act = (TextView) itemView.findViewById(R.id.activity_data);
            de = (TextView) itemView.findViewById(R.id.des_data);
            co = (TextView) itemView.findViewById(R.id.cost_data);
            button = (Button) itemView.findViewById(R.id.donate);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.servicecostcard, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.act.setText(activity[position]);
        viewHolder.co.setText(cost[position]);
        viewHolder.de.setText(des[position]);
        viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Give Donation");
                builder.setMessage("Are you sure to donate ?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Intent i = new Intent(context, SignUpActivity.class);
//                        context.startActivity(i);
                        Toast.makeText(context,"You will be redirected to payment gateway",Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return activity.length;
    }
}
