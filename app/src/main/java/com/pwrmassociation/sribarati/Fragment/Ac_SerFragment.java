package com.pwrmassociation.sribarati.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pwrmassociation.sribarati.Adapter.ServiceCostAdapter;
import com.pwrmassociation.sribarati.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Ac_SerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Ac_SerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Ac_SerFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RecyclerView recyclerView;

    private OnFragmentInteractionListener mListener;

    String activity[] ={"Oil for lighting lamps in temple","Nithya pooja Assistance","Veda patasala","One day meals",
    "Dwadasi feeding","Gho sala maintenance","Veda parayanam","Gho dhanam","Srardam performance"};



    String cost[] = {"Rs. 100/-","Rs. 500/-","Your offering","Rs. 2500/-","Rs. 200/-","Rs. 4000/-","Your offering","Your offering","Your offering"};



    String des[] = {"Adopt a temple and help light glow throughout the month",
            "Adopt a temple and help light glow throughout the month","",
            "Veda patasala students at Shree Bala Veda Pada sala, Acharapakkam, in the chennai suburbs",
    "Dwadasi feeding of Brahmins","Attached to veda patasala and one near Madurantagam having more than 400 cows. " +
            "any amount/or Rs 4000 for one time feeding of agathi keerai to all the 400 cows on a single day say Amavasai - you can personall attend and distribute",
    "Veda parayanam at your place by the students can also be arranged","Gho dhanam can also be arranged on a case to case basis",
            "place and food can be arranged at a centre near virugambakkam\n" +
            "performance of Any homam can be arranged"};


    String[] TempleName = {"இலங்கை பத்திரிகைகள்"};
    public Ac_SerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Ac_SerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Ac_SerFragment newInstance(String param1, String param2) {
        Ac_SerFragment fragment = new Ac_SerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_ac_, container, false);

        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);

        ServiceCostAdapter serviceCostAdapter = new ServiceCostAdapter(getContext(), activity,cost,des);
        recyclerView.setAdapter(serviceCostAdapter);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
